<!DOCTYPE html>
<?php
// setcookie('score', '0', time() + 3600);
session_start();
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page 3</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>
<style>
    .form__result {
        margin: 60px auto;
        max-width: 500px;
        border: 1px solid #ccc;
        box-shadow: 4px 4px 2px rgba(0,0,0,0.3);
        border-radius: 20px;
    }
    .form__content {
        text-align: center;
        padding: 20px;
        
    }
    .form__icon{
        color: #6dc41f;
        font-size: 60px;
    }
    .form__title {
        font-size: 20px;
        margin-top: 10px;
    }
    .form__score {
        margin-top: 10px;
    }
    .form__review {
        font-size: 18px;
        font-weight: 600;
    }
</style>
<body>
    <div class="form__result">
        <div class="form__content">
            <i class="form__icon fa-solid fa-circle-check"></i>
            <div class="form__title">Your finall score is:</div>
            <h1 class="form__score">
                <?php
                    echo $_SESSION['score'];
                ?>
            </h1>
            <p class="form__review">
                <?php
                $score = $_SESSION['score'];
                if ($score < 4) {
                    echo 'Bạn quá kém, cần ôn tập thêm nhé!';
                }else if($score >= 4 && $score <= 7) {
                    echo 'Cũng bình thường thôi <';
                }else {
                    echo 'Sắp sửa làm được trợ giảng lớp PHP rồiii';
                }
                ?>
            </p>
        </div>
    </div>
    
</body>
</html>
