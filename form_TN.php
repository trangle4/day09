<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bài trắc nghiệm</title>
    <link rel="stylesheet" href="edit.css">
    
    
</head>
<body>
                 
    <?php
        $question = array(1 => " Câu 1: Hình lập phương có bao nhiêu cạnh?",
                            2 => "Câu 2: Nghiệm của phương trình sin(2x) = 0 là:",
                            3 => "Câu 3: Công thức tính thể tích của hình nón tròn xoay là:",
                            4 => "Câu 4: Từ các số 0, 1, 2, 3, 4,5 số cách lập được số tự nhiên chẵn có 5 chữ số khác nhau la",
                            5 => "Câu 5 : Các mặt bên của tứ diện đều là hình gi?");
        
            
        $Answer = array (
                    1 => array( "12 canh"=> 1,
                                "8 cạnh" => 0,
                                "6 cạnh" => 0,
                                "4 cạnh" => 0),
                    2 => array ("x = pi/3" => 0,
                                "x = pi/4" => 0,
                                "x = pi/5" => 0,
                                "x = pi"   => 1),
                    3 => array ("V = 1/3*B*h"=> 1,
                                "V = B*h"   => 0,
                                "V = 1/4*B/h"=> 0,
                                "V = 1/6*B*h" => 0),

                    4 => array( 1235 => 0,
                                1500 => 0,
                                1120 => 0,
                                3012 => 1),
                    5 => array ("Tứ giác" => 0,
                                "Tam giác vuông" => 0,
                                "Tam giác đều" => 1,
                                "Hình vuông" => 0)

        );

        foreach ( $question as $keyquestion => $valuequestion ) {
            ?>
                <b class=""><?php echo "$valuequestion <br>"; ?> </b>
            <?php 
            foreach ($Answer as $keyanswer => $valueanswer) {
                if ($keyquestion == $keyanswer){
                    foreach ( $valueanswer as $key =>$value) {
                        ?>
                        <input type="radio" name="<?php echo $keyanswer ;?>" value="<?php echo "$key <br>"; ?>">
                        <?php
                        echo "$key <br>";   
                    }
                }    
            }
            echo "<br>";
        }
        
        
        

    ?>
        
</body>
</html>